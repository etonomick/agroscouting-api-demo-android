package com.etonomick.agroscouting;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

@SuppressLint("StaticFieldLeak")
class Requests extends ViewModel {

    private static Requests instance;
    public RequestQueue requestQueue;

    static Requests getInstance(Context context) {
        if (instance == null) {
            instance = new Requests(context);
        }
        return instance;
    }

    private Requests(Context context) {
        requestQueue = Volley.newRequestQueue(context);
    }

}
