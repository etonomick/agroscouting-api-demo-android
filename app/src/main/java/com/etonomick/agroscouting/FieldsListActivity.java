package com.etonomick.agroscouting;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.concurrent.ExecutionException;

@SuppressLint("ViewHolder")
public class FieldsListActivity extends AppCompatActivity {

    RequestQueue requestQueue;

    ListView fieldsListView;
    BaseAdapter fieldsListBaseAdapter;
    JSONArray fields;

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestQueue = Requests.getInstance(getApplicationContext()).requestQueue;

        fields = new JSONArray();

        fieldsListView = findViewById(R.id.list_view_fields);
        fieldsListBaseAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return fields.length();
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View row = getLayoutInflater().inflate(android.R.layout.simple_list_item_2, parent, false);

                TextView textView = row.findViewById(android.R.id.text1);
                try {
                    textView.setText(fields.getJSONObject(position).getString("field_name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                TextView detailTextView = row.findViewById(android.R.id.text2);
                try {
                    detailTextView.setText(fields.getJSONObject(position).getString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return row;
            }
        };
        fieldsListView.setAdapter(fieldsListBaseAdapter);
        fieldsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(FieldsListActivity.this, FieldDetailsActivity.class);
                try {
                    intent.putExtra("field_id", fields.getJSONObject(position).getString("id"));
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        progressBar = findViewById(R.id.progress_bar);

        requestQueue.add(new JsonArrayRequest(
                Request.Method.POST,
                getResources().getString(R.string.demo_api_url),
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        fields = response;
                        fieldsListBaseAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                return "{\"method\":\"get_fields\"}".getBytes();
            }
        });
    }
}
