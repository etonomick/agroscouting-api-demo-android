package com.etonomick.agroscouting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

public class FieldDetailsActivity extends AppCompatActivity {

    String fieldId;
    HashMap<String, String> field;
    RequestQueue requestQueue;

    ListView fieldDetailsListView;
    BaseAdapter fieldDetailsBaseAdapter;

    ProgressBar progressBar;

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_field_details);

        fieldId = getIntent().getStringExtra("field_id");

        field = new HashMap<>();

        requestQueue = Requests.getInstance(getApplicationContext()).requestQueue;

        fieldDetailsListView = findViewById(R.id.list_view_field_detail);
        fieldDetailsBaseAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return field.size();
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View row = getLayoutInflater().inflate(android.R.layout.simple_list_item_2, parent, false);

                String key = (String) field.keySet().toArray()[position];

                TextView textView = row.findViewById(android.R.id.text1);
                textView.setText(field.get(key));

                TextView detailTextView = row.findViewById(android.R.id.text2);
                detailTextView.setText(key);

                return row;
            }
        };
        fieldDetailsListView.setAdapter(fieldDetailsBaseAdapter);

        progressBar = findViewById(R.id.progress_bar);

        requestQueue.add(new JsonObjectRequest(
                Request.Method.POST,
                getResources().getString(R.string.demo_api_url),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Iterator iterator = response.keys();
                        while (iterator.hasNext()) {
                            String key = iterator.next().toString();
                            try {
                                field.put(key, response.getString(key));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        progressBar.setVisibility(View.GONE);
                        fieldDetailsBaseAdapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() {
                String requestBody = "{\"method\":\"get_field\",\"id\":" + fieldId + "}";
                return requestBody.getBytes();
            }
        });
    }
}
